import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    padding: 15,
    backgroundColor: '#f5f5f7'
  },
  jobSection: {
    backgroundColor: '#ffffff',
    borderRadius: 3
  },
  jobTitleSection: {
    flex: 1,
    flexDirection: 'row',
    padding: 15
  },
  job: {
    flex: 3,
    padding: 3
  },
  customer: {
    flex: 4,
    padding: 3
  },
  jobTitle: {
    color: '#646771',
    fontSize: 14,
    lineHeight: 16,
    textAlign: 'left',
    fontFamily: 'Roboto-Regular'
  },
  jobTitleDesp: {
    fontFamily: 'Roboto-Medium',
    color: '#003c7a',
    fontSize: 16,
    lineHeight: 19,
    textAlign: 'left'
  },
  jobContent: {
    fontFamily: 'Roboto-Regular',
    color: '#004581',
    fontSize: 15,
    lineHeight: 20,
    textAlign: 'left'
  },
  address: {
    flex: 3,
    padding: 3
  },
  contact: {
    flex: 4,
    flexDirection: 'row',
    padding: 3
  },
  enterBySection: {
    marginTop: 10,
    backgroundColor: '#ffffff',
    flex:1,
    flexDirection: 'row',
    padding: 15,
    borderRadius: 3
  },
  enterBy: {
    flex: 3,
    padding: 3
  },
  enterByInput: {
    color: '#141823',
    paddingTop: 12,
    paddingBottom: 10,
    paddingLeft: 10,
    backgroundColor: '#eeeef2',
    fontSize: 15,
    lineHeight: 18,
    marginTop: 5,
    borderRadius: 5
  },
  regard: {
    flex: 4,
    padding: 3
  },
  regardInput: {
    color: '#141823',
    paddingTop: 12,
    paddingBottom: 10,
    paddingLeft: 10,
    fontSize: 15,
    lineHeight: 18,
    marginTop: 5,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: '#ccced5'
  },
  commentsSection: {
    marginTop: 10,
    padding: 15,
    backgroundColor: '#ffffff'
  },
  commentTitleContainer: {
    flexDirection: 'row',
    paddingBottom: 12
  },
  commentsTitle: {
    flex: 3,
    color: '#141823',
    fontSize: 18,
    lineHeight: 21,
    fontFamily: 'Roboto-Medium',
    fontWeight: '500'
  },
  addIcon: {
    width: 18,
    height: 18
  },
  itemContent: {
    borderTopWidth: 1,
    borderTopColor: '#e7e8ed',
    paddingTop: 15,
    paddingBottom: 15
  },
  itemBold: {
    fontFamily: 'Roboto-Medium',
    fontWeight: '500'
  },
  itemText: {
    color: '#141823',
    fontSize: 15,
    lineHeight: 22
  },
  itemComment: {
    marginTop: 12,
    color: '#141823',
  },
  commentName: {
    fontFamily: 'Roboto-Medium',
    fontWeight: '500'
  },
  textUnderline: {
    textDecorationLine: 'underline'
  },
  images: {
    flex: 1,
    flexDirection: 'row'
  },
  img: {
    width: 161,
    height: 109,
    marginRight: 15,
    borderRadius: 5
  }
});

export default styles;