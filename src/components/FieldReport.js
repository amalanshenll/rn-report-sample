import React from 'react';
import {
  SafeAreaView,
  ScrollView,
  View,
  Text,
  TextInput,
  Image
} from 'react-native';
import styles from './styles';
import addIcon from '../assets/icons/add.png';
import img1 from '../assets/images/img1.jpg';
import img2 from '../assets/images/img2.jpg';
import img3 from '../assets/images/img3.jpg';

const FieldReport = () => {
  return (
    <SafeAreaView>
      <ScrollView
        contentInsetAdjustmentBehavior="automatic"
      >
        <View style={styles.container}>
          <View style={styles.jobSection}>
            <View style={styles.jobTitleSection}>
              <View style={styles.job}>
                <Text style={styles.jobTitle}>Job Name</Text>
                <Text style={styles.jobTitleDesp}>Tilt-up gate on Large garage - chain broken</Text>
              </View>
              <View style={styles.customer}>
                <Text style={styles.jobTitle}>Customer</Text>
                <Text>
                  <Text style={styles.jobTitleDesp}>Lao Family Community Deve</Text>
                  <Text>{' '}</Text>
                  <Text style={styles.jobContent}>(Fountain Blue Apartments - 305 Euclid)</Text>
                </Text>
              </View>
            </View>
            <View style={styles.jobTitleSection}>
              <View style={styles.address}>
                <Text style={styles.jobTitle}>Address</Text>
                <Text style={styles.jobContent}>2837 East Wilmington</Text>
                <Text style={styles.jobContent}>Suite 4000</Text>
                <Text style={styles.jobContent}>Rancho Cucamonga, CA 90825</Text>
              </View>
              <View style={styles.contact}>
                <View style={{flex: 1}}>
                  <Text style={styles.jobTitle}>Contact</Text>
                  <Text style={styles.jobContent}>Tom Brady</Text>
                  <Text style={styles.jobContent}>(562) 555-1514</Text>
                </View>
                <View style={{flex: 1}}>
                  <Text style={styles.jobTitle}>On-Site</Text>
                  <Text style={styles.jobContent}>Robert Louis Stevenson</Text>
                  <Text style={styles.jobContent}>(562) 555-2626</Text>
                </View>
              </View>
            </View>
          </View>
          <View style={styles.enterBySection}>
            <View style={styles.enterBy}>
              <Text style={styles.jobTitle}>Entered By</Text>
              <TextInput style={styles.enterByInput} value={'Robert Louis, 7/22/2020'}/>
            </View>
            <View style={styles.regard}>
              <Text style={styles.jobTitle}>Regarding</Text>
              <TextInput style={styles.regardInput} value={'Site Visit'}/>
            </View>
          </View>
          <View style={styles.commentsSection}>
            <View style={styles.commentTitleContainer}>
              <Text style={styles.commentsTitle}>Comments</Text>
              <View>
                <Image style={styles.addIcon} source={addIcon} />
              </View>
            </View>
            <View style={styles.itemContent}>
              <Text>
                <Text style={styles.itemBold}>Item #1:</Text>
                <Text style={styles.itemText}>{'Went thru all the material on site for the doors on the 1st level. Found everything accounted for except for 2 fire door operators for doors L27 & L 28 Cookson Job #188784784.'}</Text>
              </Text>
              <Text style={styles.itemComment}>
                <Text>Emailed</Text>
                <Text style={styles.commentName}> John Carl </Text>
                <Text>and informed him of missing material, he emailed me back that those items were </Text>
                <Text style={styles.textUnderline}>originally short shipped at a later date to Certified Door.</Text>
              </Text>
            </View>
            <View style={styles.itemContent}>
              <Text>
                <Text style={styles.itemBold}>Item #2:</Text>
                <Text style={styles.itemText}>{' The motors are listed as Model FDO RM. Also found no controls for doors on 1st floor. '}</Text>
              </Text>
              <Text style={styles.itemComment}>
                <Text>Shipments would be sent to</Text>
                <Text style={styles.commentName}> Marine Technical Services </Text>
                <Text>211 N. Marine Ave Willmington, CA 90744</Text>
              </Text>
            </View>
          </View>
          <View style={styles.commentsSection}>
            <View style={styles.commentTitleContainer}>
              <Text style={styles.commentsTitle}>Attachments</Text>
              <View>
                <Image style={styles.addIcon} source={addIcon} />
              </View>
            </View>
            <View style={styles.itemContent}>
              <View style={styles.images}>
                <Image source={img1} style={styles.img} />
                <Image source={img2} style={styles.img} />
                <Image source={img3} style={styles.img} />
              </View>
            </View>
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  )
}

export default FieldReport;